package net.drthum.scalacraft.utils

import java.math.BigInteger
import java.security.SecureRandom

class BigNumber(bigInteger: BigInteger) {
  def this() = this(BigInteger.ZERO)

  def getBigInteger() = bigInteger.abs()
  def +(other: BigNumber) = new BigNumber(bigInteger.add(other.getBigInteger))
  def *(other: BigNumber) = new BigNumber(bigInteger.multiply(other.getBigInteger))
  def %(modulo: BigNumber) = new BigNumber(bigInteger.mod(modulo.getBigInteger))
  def modPow(exponent: BigNumber, modulo: BigNumber) = new BigNumber(bigInteger.modPow(exponent.getBigInteger, modulo.getBigInteger))

  def asByteArray(minSize: Int) = {
    var array = bigInteger.toByteArray
    if (array(0) == 0) {
      val tmp: Array[Byte] = new Array[Byte](array.length - 1)
      System.arraycopy(array, 1, tmp, 0, tmp.length)
      array = tmp
    }

    array = array.reverse

    if (minSize > array.length) {
      val newArray: Array[Byte] = new Array[Byte](minSize)
      System.arraycopy(array, 0, newArray, 0, array.length)
      newArray
    } else
      array
  }

  def asHexStr() = bigInteger.toString(16)
}

object BigNumber {
  val rng = new SecureRandom()
  def setHexStr(str: String) = new BigNumber(new BigInteger(str, 16))

  def setRand(numBytes: Int) = {
    val array = new Array[Byte](numBytes)
    rng.nextBytes(array)
    new BigNumber(new BigInteger(1, array))
  }

  def setBinary(pArray: Array[Byte]) = {
    var array = pArray.reverse
    val length = array.length

    if (array(0) < 0) {
      val tmp = new Array[Byte](length + 1)
      System.arraycopy(array, 0, tmp, 1, length)
      array = tmp
    }

    new BigNumber(new BigInteger(array))
  }

  def apply() = new BigNumber
}