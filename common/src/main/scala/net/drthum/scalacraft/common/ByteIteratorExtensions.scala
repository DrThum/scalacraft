package net.drthum.scalacraft.common

import akka.util.{ByteString, ByteIterator}
import scala.annotation.tailrec

object ByteIteratorExtensions {

  implicit class ByteIteratorExtensions(iter: ByteIterator) {
    def getString(length: Int): String = {
      val bytes = new Array[Byte](length)
      iter.getBytes(bytes)
      ByteString(bytes).utf8String
    }

    def getBytes(length: Int): Array[Byte] = {
      val bytes = new Array[Byte](length)
      iter.getBytes(bytes)
      bytes
    }

    def getNullTeminatedString(): String = {
      @tailrec
      def step(acc: StringBuilder): String = {
        val next = iter.getByte
        if (next == 0)
          acc.result
        else
          step(acc.append(next.toChar))
      }

      step(new StringBuilder)
    }
  }

}
