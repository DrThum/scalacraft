package net.drthum.scalacraft.common

import java.security.MessageDigest

import net.drthum.scalacraft.utils.BigNumber

object CryptoConstants {
  val N = BigNumber.setHexStr("894B645E89E1535BBDAD5B8B290650530801B18EBFBF5E8FAB3C82872A3E9BB7")
  val NHash = MessageDigest.getInstance("SHA-1").digest(N.asByteArray(32))
  val g = BigNumber.setHexStr("7")
  val gHash = MessageDigest.getInstance("SHA-1").digest(g.asByteArray(1))
  val k = BigNumber.setHexStr("3")
}
