package entities

import slick.driver.PostgresDriver.api._
import slick.lifted.Tag

case class Account(id: Long, username: String, password: String, sessionKey: String, v: String, s: String)

class Accounts(tag: Tag)
  extends Table[Account](tag, "accounts") {

  def id = column[Long]("id", O.PrimaryKey)
  def username = column[String]("username")
  def password = column[String]("password")
  def sessionkey = column[String]("sessionkey")
  def v = column[String]("v")
  def s = column[String]("s")

  def * = (id, username, password, sessionkey, v, s) <> (Account.tupled, Account.unapply)

}
