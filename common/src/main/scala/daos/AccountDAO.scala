package daos

import entities.Accounts
import services.DBService
import slick.driver.PostgresDriver.api._

import scala.concurrent.ExecutionContext.Implicits.global

object AccountDAO {
  val db = DBService.authDB
  val accounts = TableQuery[Accounts]

  def findByUsername(username: String) = db.run(accounts.filter(_.username === username).result).map(_.headOption)

  def updateSessionKey(id: Long, key: String, v: String, s: String) = db.run(accounts.filter(_.id === id).map(a => (a.sessionkey, a.v, a.s)).update(key, v, s))
}
