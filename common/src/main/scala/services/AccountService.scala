package services

import daos.AccountDAO
import entities.Account
import scala.concurrent.Future

object AccountService {
  def findByUsername(username: String): Future[Option[Account]] = AccountDAO.findByUsername(username)

  def updateKeys(id: Long, key: String, v: String, s: String): Future[Int] = AccountDAO.updateSessionKey(id, key, v, s)
}
