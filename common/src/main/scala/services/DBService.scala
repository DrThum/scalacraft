package services

import slick.driver.PostgresDriver.api._

object DBService {
  val authDB = Database.forConfig("db.auth")

  def shutdown = {
    authDB.close
  }
}

