package net.drthum.scalacraft.proxy

import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

import net.drthum.scalacraft.utils.BigNumber

/* WARNING: NOT thread-safe */
class ProxyCrypto {
  private var _key: Array[Byte] = Array.empty
  private var init = false
  private val tbcKey: Array[Byte] = Array(0x38, 0xA7, 0x83, 0x15, 0xF8, 0x92, 0x25, 0x30,
    0x71, 0x98, 0x67, 0xB1, 0x8C, 0x04, 0xE2, 0xAA).map(_.toByte)
  private var recv_i = 0
  private var recv_j = 0
  private var send_i = 0
  private var send_j = 0

  def setKey(key: BigNumber) = {
    val m = Mac.getInstance("HmacSHA1")
    m.init(new SecretKeySpec(tbcKey, "HmacSHA1"))
    _key = m.doFinal(key.asByteArray(40).reverse)
    init = true
  }

  def encrypt(data: Array[Byte]): Array[Byte] = {
    if (!init)
      data
    else {
      val ret = data.clone()
      for (t <- 0 to 3) {
        send_i %= 20
        val x = ((data(t) ^ _key(send_i)) + send_j).toByte
        send_i = send_i + 1
        send_j = x
        ret(t) = send_j.toByte
      }
      ret
    }
  }

  def decrypt(data: Array[Byte]): Array[Byte] = {
    if (!init)
      data
    else {
      val ret = data.clone()
      for (t <- 0 to 5) {
        recv_i %= 20
        val x = ((data(t) - recv_j) ^ _key(recv_i)).toByte
        recv_i = recv_i + 1
        recv_j = data(t)
        ret(t) = x
      }
      ret
    }
  }
}
