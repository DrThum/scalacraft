package net.drthum.scalacraft.proxy

import java.math.BigInteger
import java.nio.ByteBuffer
import java.nio.ByteOrder._
import java.nio.charset.Charset
import java.security.MessageDigest

import akka.actor._
import akka.io.Tcp.{Write, PeerClosed, Received}
import akka.util.{ByteString, ByteStringBuilder}
import net.drthum.scalacraft.common.CryptoConstants
import net.drthum.scalacraft.common.ByteIteratorExtensions._
import net.drthum.scalacraft.common.Opcodes._
import net.drthum.scalacraft.utils.BigNumber
import services.AccountService
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Random

class ProxySocket(remoteAddress: String, connection: ActorRef) extends Actor with ActorLogging {
  implicit val byteOrder = nativeOrder
  private val seed = Random.nextInt
  private val crypto = new ProxyCrypto
  sendAuthChallenge()

  def receive: Receive = {
    case Received(data) => {
      val itr = data.iterator
      val header = ByteString(crypto.decrypt(itr.getBytes(6)))
      val itrhdr = header.iterator

      val size = itrhdr.getShort(BIG_ENDIAN)
      val opcode = itrhdr.getShort
      val socket = sender

      // TODO: Handle the case where an incomplete packet was received
      if (data.length > (size + 2))
        self ! Received(data.drop(size + 2))
      // TODO: Ensure that this message is processed right after this one (= enqueued first)

      opcode match {
        case CMSG_AUTH_SESSION => {
          val clientBuild = itr.getInt
          itr.drop(4)
          val username = itr.getNullTeminatedString()
          val clientSeed = itr.getInt
          val clientDigest = itr.getBytes(20)

          AccountService.findByUsername(username).map(_.map { account =>
            log.info(s"User ${account.username} tries to log in from $remoteAddress")

            val sBin = BigNumber.setHexStr(account.s).asByteArray(32).reverse
            val passwordHash = BigNumber.setHexStr(account.password).asByteArray(20).reverse
            val sha = MessageDigest.getInstance("SHA-1")
            sha.update(sBin)
            sha.update(passwordHash)
            val digest = sha.digest()
            val x = BigNumber.setBinary(digest)
            val v = BigNumber.setBinary(CryptoConstants.g.modPow(x, CryptoConstants.N).asByteArray(32).reverse)

            // Check that the client went through the authserver
            val calculatedV = account.v.startsWith("0") match {
              case true => "0" + v.asHexStr()
              case false => v.asHexStr()
            }

            if (account.v == calculatedV) {
              val K = BigNumber.setHexStr(account.sessionKey)
              // Check that it's correctly authenticated
              val kBin = K.asByteArray(40).reverse

              sha.reset()
              sha.update(username.getBytes())
              sha.update(Array(0.toByte, 0.toByte, 0.toByte, 0.toByte))
              sha.update(Array(clientSeed.toByte, (clientSeed >> 8).toByte, (clientSeed >> 16).toByte, (clientSeed >> 24).toByte))
              sha.update(Array(seed.toByte, (seed >> 8).toByte, (seed >> 16).toByte, (seed >> 24).toByte))
              val checkDigest = sha.digest(kBin)

              if (checkDigest.take(20).sameElements(clientDigest.take(20))) {
                crypto.setKey(K)
                val builder = new ByteStringBuilder
                builder.putBytes(
                  crypto.encrypt(
                    new ByteStringBuilder().putShort(13)(BIG_ENDIAN).putShort(SMSG_AUTH_RESPONSE).result().toArray
                  )
                )
                builder.putByte(0x0C.toByte) // AUTH_OK
                builder.putInt(0)
                builder.putByte(0.toByte)
                builder.putInt(0)
                builder.putByte(1.toByte)
                socket ! Write(builder.result)
              } else {
                val builder = new ByteStringBuilder
                builder.putBytes(
                  crypto.encrypt(
                    new ByteStringBuilder().putShort(1)(BIG_ENDIAN).putShort(SMSG_AUTH_RESPONSE).result().toArray
                  )
                )
                builder.putByte(0x0D.toByte) // AUTH_FAILED
                socket ! Write(builder.result)
                log.error(s"User ${account.username} from $remoteAddress: client digest doesn't match")
                context stop self
              }
            } else {
              val builder = new ByteStringBuilder
              builder.putBytes(
                crypto.encrypt(
                  new ByteStringBuilder().putShort(1)(BIG_ENDIAN).putShort(SMSG_AUTH_RESPONSE).result().toArray
                )
              )
              builder.putByte(0x15.toByte) // AUTH_UNKNOWN_ACCOUNT
              socket ! Write(builder.result)
              log.error(s"User ${account.username} from $remoteAddress: v hex strings don't match")
              context stop self
            }
          })
        }
        case CMSG_PING => {
          val seq = itr.getInt
          val builder = new ByteStringBuilder
          builder.putBytes(
            crypto.encrypt(
              new ByteStringBuilder().putShort(6)(BIG_ENDIAN).putShort(SMSG_PONG).result().toArray
            )
          )
          builder.putInt(seq)
          socket ! Write(builder.result)
        }
        case other => {
          log.info(s"Received unhandled opcode 0x${Integer.toHexString(opcode)} with size $size (actual message size = ${data.length})")
        }
      }
    }
    case PeerClosed => {
      log.debug(s"Client disconnected ($remoteAddress)")
      context stop self
    }
  }

  private def sendAuthChallenge() = {
    val builder = new ByteStringBuilder()
      .putShort(6)(BIG_ENDIAN)
      .putShort(SMSG_AUTH_CHALLENGE)
      .putInt(seed)
    connection ! Write(builder.result)
  }

}
