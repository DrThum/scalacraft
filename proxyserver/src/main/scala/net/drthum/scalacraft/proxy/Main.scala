package net.drthum.scalacraft.proxy

import scala.concurrent.ExecutionContext.Implicits.global

import akka.actor.{Props, ActorSystem}
import com.typesafe.config.ConfigFactory

object Main extends App {
  val system = ActorSystem("ProxySystem")
  val server = system.actorOf(Props[ProxyServer], "server")
}
