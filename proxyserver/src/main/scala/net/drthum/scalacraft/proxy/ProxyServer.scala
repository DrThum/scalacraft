package net.drthum.scalacraft.proxy

import akka.actor._
import akka.io._
import akka.io.Tcp._

import java.net.InetSocketAddress

class ProxyServer extends Actor with ActorLogging {
  import context.system

  IO(Tcp) ! Bind(self, new InetSocketAddress("0.0.0.0", 8085))

  def receive = {
    case Bound(localAddress) => {
      log.info(s"ProxyServer listening on ${localAddress.getHostString}:${localAddress.getPort}")
    }
    case CommandFailed(bind: Bind) => {
      log.error(s"Failed to bind on ${bind.localAddress.getHostString}:${bind.localAddress.getPort}")
      context stop self
    }
    case Connected(remote, _) => {
      log.debug(s"Client connecting from ${remote.getHostString}:${remote.getPort}")
      val handler = context.actorOf(Props(new ProxySocket(remote.getHostString, sender)), s"proxy-player-${remote.getHostString.replace('.', '-')}-${remote.getPort}")
      sender ! Register(handler)
    }
  }
}
