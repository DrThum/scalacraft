name := "ScalaCraft Authentication Server"

oneJarSettings

mainClass in oneJar := Some("net.drthum.scalacraft.auth.Main")
