package net.drthum.scalacraft.services

import scala.concurrent.Future

import net.drthum.scalacraft.daos.RealmDAO
import net.drthum.scalacraft.entities.Realm

object RealmService {
  def listAllRealms(): Future[Seq[Realm]] = RealmDAO.all
}
