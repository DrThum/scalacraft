package net.drthum.scalacraft.entities

import slick.lifted.Tag
import slick.driver.PostgresDriver.api._

case class Realm(id: Long, name: String, address: String, icon: Int, locked: Boolean, flags: Int, population: Double, timezone: Int)

class Realms(tag: Tag)
    extends Table[Realm](tag, "realms") {

  def id = column[Long]("id", O.PrimaryKey)
  def name = column[String]("name")
  def address = column[String]("address")
  def icon = column[Int]("icon")
  def locked = column[Boolean]("locked")
  def flags = column[Int]("flags")
  def population = column[Double]("population")
  def timezone = column[Int]("timezone")

  def * = (id, name, address, icon, locked, flags, population, timezone) <> (Realm.tupled, Realm.unapply)

}
