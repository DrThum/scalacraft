package net.drthum.scalacraft.auth

import net.drthum.scalacraft.utils.BigNumber
import java.security.MessageDigest

object AuthConstants {
  object Commands {
    val AUTH_LOGON_CHALLENGE: Byte  = 0x00
    val AUTH_LOGON_PROOF: Byte      = 0x01
    val REALM_LIST: Byte            = 0x10
  }

  object Results {
    val REALM_AUTH_SUCCESS: Byte              = 0x00
    val REALM_AUTH_FAILURE: Byte              = 0x01                 ///< Unable to connect
    val REALM_AUTH_UNKNOWN1: Byte             = 0x02                 ///< Unable to connect
    val REALM_AUTH_ACCOUNT_BANNED: Byte       = 0x03                 ///< This game account has been closed and is no longer available for use. Please go to site/banned.html for further information.
    val REALM_AUTH_NO_MATCH: Byte             = 0x04                 ///< The information you have entered is not valid. Please check the spelling of the account name and password. If you need help in retrieving a lost or stolen password, see site for more information
    val REALM_AUTH_UNKNOWN2: Byte             = 0x05                 ///< The information you have entered is not valid. Please check the spelling of the account name and password. If you need help in retrieving a lost or stolen password, see site for more information
    val REALM_AUTH_ACCOUNT_IN_USE: Byte       = 0x06                 ///< This account is already logged into game. Please check the spelling and try again.
    val REALM_AUTH_PREPAID_TIME_LIMIT: Byte   = 0x07                 ///< You have used up your prepaid time for this account. Please purchase more to continue playing
    val REALM_AUTH_SERVER_FULL: Byte          = 0x08                 ///< Could not log in to game at this time. Please try again later.
    val REALM_AUTH_WRONG_BUILD_NUMBER: Byte   = 0x09                 ///< Unable to validate game version. This may be caused by file corruption or interference of another program. Please visit site for more information and possible solutions to this issue.
    val REALM_AUTH_UPDATE_CLIENT: Byte        = 0x0a                 ///< Downloading
    val REALM_AUTH_UNKNOWN3: Byte             = 0x0b                 ///< Unable to connect
    val REALM_AUTH_ACCOUNT_FROZEN: Byte       = 0x0c                 ///< This game account has been temporarily suspended. Please go to site/banned.html for further information
    val REALM_AUTH_UNKNOWN4: Byte             = 0x0d                 ///< Unable to connect
    val REALM_AUTH_UNKNOWN5: Byte             = 0x0e                 ///< Connected.
    val REALM_AUTH_PARENTAL_CONTROL: Byte     = 0x0f                 ///< Access to this account has been blocked by parental controls. Your settings may be changed in your account preferences at site
    val REALM_AUTH_ACCOUNT_LOCKED: Byte       = 0x10                  ///< This account is locked(...)
  }
}