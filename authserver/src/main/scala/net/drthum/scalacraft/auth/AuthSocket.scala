package net.drthum.scalacraft.auth

import java.util.Arrays
import java.nio.ByteOrder
import java.security.MessageDigest

import entities.Account
import net.drthum.scalacraft.auth.packet.{AuthLogonProof, AuthLogonChallenge}
import net.drthum.scalacraft.services.RealmService
import services.AccountService

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext.Implicits.global

import akka.actor._
import akka.io.Tcp._
import akka.util.{ByteStringBuilder, ByteString, ByteIterator}

import AuthConstants._
import net.drthum.scalacraft.utils.BigNumber
import net.drthum.scalacraft.common.CryptoConstants

class AuthSocket(remoteAddress: String) extends Actor with ActorLogging {
  implicit val byteOrder = ByteOrder.nativeOrder

  def socket = sender

  def receive = notConnected

  def notConnected: Receive = {
    case Received(data) => data.head match {
      case Commands.AUTH_LOGON_CHALLENGE => AuthLogonChallenge(data).map(handleLogonChallenge(_)).getOrElse(logAndStop("Received malformed AUTH_LOGON_CHALLENGE"))
      case head =>
        log.error(s"Unhandled command $head in state NOT_CONNECTED (expected AUTH_LOGON_CHALLENGE)")
        context stop self
    }
    case PeerClosed => {
      log.debug(s"Client disconnected ($remoteAddress)")
      context stop self
    }
  }

  def connected(account: Account, srpValues: SRPValues): Receive = {
    case Received(data) => data.head match {
      case Commands.AUTH_LOGON_PROOF => AuthLogonProof(data).map(handleLogonProof(_, account, srpValues)).getOrElse(logAndStop("Received malformed AUTH_LOGON_PROOF"))
      case head =>
        log.error(s"Unhandled command $head in state CONNECTED (expected AUTH_LOGON_PROOF)")
        context stop self
    }
    case PeerClosed => {
      log.debug(s"Client disconnected ($remoteAddress)")
      context stop self
    }
  }

  def authenticated: Receive = {
    case Received(data) => data.head match {
      case Commands.REALM_LIST => handleRealmList()
      case head =>
        log.error(s"Unhandled command $head in state AUTHENTICATED (expected REALM_LIST)")
        context stop self
    }
    case PeerClosed => {
      log.debug(s"Client disconnected ($remoteAddress)")
      context stop self
    }
  }

  private def handleLogonChallenge(packet: AuthLogonChallenge): Unit = {
    val socket = sender // Override socket because the following code is asynchronous

    AccountService.findByUsername(packet.username).map(_.map { acc =>
      log.info(s"User ${acc.username} tries to log in from $remoteAddress")

      if (packet.build != 8606) {
        socket ! Write(ByteString(Commands.AUTH_LOGON_CHALLENGE, 0, Results.REALM_AUTH_WRONG_BUILD_NUMBER))
        context stop self
      } else {
        val srpValues = new SRPValues().initVS(acc.password).initB()
        val builder = new ByteStringBuilder
        builder.putByte(Commands.AUTH_LOGON_CHALLENGE)
        builder.putByte(0)
        builder.putByte(Results.REALM_AUTH_SUCCESS)
        builder.putBytes(srpValues.B.asByteArray(32))
        builder.putByte(1)
        builder.putBytes(CryptoConstants.g.asByteArray(1))
        builder.putByte(32)
        builder.putBytes(CryptoConstants.N.asByteArray(32))
        builder.putBytes(srpValues.s.asByteArray(32))
        val unk3 = BigNumber.setRand(16)
        builder.putBytes(unk3.asByteArray(16))
        builder.putByte(0) // Security flags
        // TODO: authenticator
        socket ! Write(builder.result)

        context.become(connected(acc, srpValues))
      }
    }.getOrElse {
      log.info(s"Login attempt to unknown account ${packet.username} from $remoteAddress")
      context stop self
    }).recover {
      case e => // TODO: Improve error logging/handling
        println(e.getMessage)
        e.printStackTrace
    }
  }

  private def handleLogonProof(packet: AuthLogonProof, account: Account, srpValues: SRPValues): Unit = {
    val sha = MessageDigest.getInstance("SHA-1")
    sha.update(packet.a)
    sha.update(srpValues.B.asByteArray(32))
    val u = BigNumber.setBinary(sha.digest())
    val A = BigNumber.setBinary(packet.a)
    val S = (A * (srpValues.v.modPow(u, CryptoConstants.N))).modPow(srpValues.b, CryptoConstants.N)

    val t = S.asByteArray(32)
    val tGrouped = t.grouped(2).toArray // Array(Array(t(0), t(1)), Array(t(2), t(3)), ...)
    val tEven = sha.digest(tGrouped.map(_(0)))
    val tOdd = sha.digest(tGrouped.map(_(1)))

    // Produce a new list containing all the elements of a and b in an alternating fashion (it's a shame that this is not present in standard lib)
    @tailrec
    def intersperse[A](a : List[A], b : List[A], acc: List[A] = Nil): List[A] = a match {
      case first :: rest => intersperse(b, rest, acc :+ first)
      case _             => acc ++ b
    }
    val vK = intersperse(tEven.toList, tOdd.toList).toArray

    val hash = CryptoConstants.NHash.zip(CryptoConstants.gHash).map {
      case (nElem, gElem) => (nElem ^ gElem).toByte
    }

    val loginHash = sha.digest(account.username.getBytes)

    sha.update(hash)
    sha.update(loginHash)
    sha.update(srpValues.s.asByteArray(32))
    sha.update(A.asByteArray(32))
    sha.update(srpValues.B.asByteArray(32))
    sha.update(vK)
    val M = sha.digest()

    if (Arrays.equals(M, packet.clientHash)) {
      log.info(s"User ${account.username} successfully authenticated from $remoteAddress")

      sha.update(packet.a)
      sha.update(M)
      sha.update(vK)
      val M2 = sha.digest()

      // Update session key in DB
      // TODO: Use the actorsystem to send this to the proxies instead
      AccountService.updateKeys(account.id,
        vK.map("%02x" format _).mkString,
        srpValues.v.asByteArray(32).map("%02x" format _).mkString,
        srpValues.s.asByteArray(32).map("%02x" format _).mkString
      )

      val builder = new ByteStringBuilder
      builder.putByte(Commands.AUTH_LOGON_PROOF.toByte)
      builder.putByte(0)
      builder.putBytes(M2)
      builder.putInt(0x800000)
      builder.putInt(0)
      builder.putShort(0)
      socket ! Write(builder.result)

      context.become(authenticated)
    } else {
      log.info(s"User ${account.username} failed to authenticate from $remoteAddress")
      socket ! Write(ByteString(Commands.AUTH_LOGON_PROOF, Results.REALM_AUTH_NO_MATCH, 3, 0))
      context stop self
    }
  }

  private def handleRealmList(): Unit = {
    val socket = sender

    // TODO: Move the realm list to an Actor and update it regularly instead of fetching it from the database every time
    RealmService.listAllRealms().map { realms =>
      val builder = new ByteStringBuilder
      builder.putInt(0)

      builder.putShort(realms.length)
      realms.foreach { realm =>
        builder.putByte(realm.icon.toByte)
        builder.putByte(if (realm.locked) 1 else 0)
        builder.putByte(realm.flags.toByte)
        builder.putBytes((realm.name + "\0").getBytes("UTF-8"))
        builder.putBytes((realm.address + "\0").getBytes("UTF-8"))
        builder.putFloat(realm.population.toFloat)
        builder.putByte(1) // Amount of characters (TODO)
        builder.putByte(realm.timezone.toByte)
        builder.putByte(0x2C) // Realm ID
      }

      builder.putByte(0x10) // Unk
      builder.putByte(0x00) // Unk
      val body = builder.result

      val packet = new ByteStringBuilder
      packet.putByte(Commands.REALM_LIST)
      packet.putShort(body.length.toShort)
      packet.append(body)

      socket ! Write(packet.result)
    }
  }

  private def logAndStop(message: String) = {
    log.error(message)
    context stop self
  }
}

case class SRPValues(B: BigNumber, b: BigNumber, s: BigNumber, v: BigNumber) {
  def this() = this(BigNumber(), BigNumber(), BigNumber(), BigNumber())

  def initB() = {
    val b = BigNumber.setRand(19)
    val gmod = CryptoConstants.g.modPow(b, CryptoConstants.N)
    this.copy(b = b, B = ((v * CryptoConstants.k) + gmod) % CryptoConstants.N)
  }

  def initVS(Ir: String) = {
    val I = BigNumber.setHexStr(Ir)
    val hash = I.asByteArray(20).reverse
    val s = BigNumber.setRand(32)

    val sha = MessageDigest.getInstance("SHA-1")
    sha.update(s.asByteArray(32))
    sha.update(hash)
    val x = BigNumber.setBinary(sha.digest())

    this.copy(s = s, v = CryptoConstants.g.modPow(x, CryptoConstants.N))
  }
}
