package net.drthum.scalacraft.auth.packet

import akka.util.ByteString
import java.nio.ByteOrder
import net.drthum.scalacraft.common.ByteIteratorExtensions._

case class AuthLogonChallenge(build: Short, username: String)

object AuthLogonChallenge {
  implicit val byteOrder = ByteOrder.nativeOrder

  def apply(bs: ByteString): Option[AuthLogonChallenge] = {
    val iter = bs.iterator
    if (iter.len >= 11 + 2 + 20 + 1) {
      iter.drop(11) // cmd (1) + error (1) + size (2) + game (4) + version1 (1) + version2 (1) + version3 (1) = 11
      val build = iter.getShort
      iter.drop(20) // platform (4) + os (4) + locale (4) + timezoneBias (4) + IP (4) = 20
      val usernameLength = iter.getByte
      if (iter.len == usernameLength) {
        val username = iter.getString(usernameLength)
        Some(AuthLogonChallenge(build, username))
      } else
        None
    } else
      None
  }
}
