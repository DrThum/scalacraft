package net.drthum.scalacraft.auth

import services.DBService

import scala.concurrent.ExecutionContext.Implicits.global

import akka.actor.{Props, ActorSystem}
import com.typesafe.config.ConfigFactory

object Main extends App {
  val config = ConfigFactory.load()

  val system = ActorSystem("AuthSystem")
  val server = system.actorOf(Props[AuthServer], "server")
}
