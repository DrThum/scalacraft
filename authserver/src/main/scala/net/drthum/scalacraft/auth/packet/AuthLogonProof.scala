package net.drthum.scalacraft.auth.packet

import akka.util.ByteString
import java.nio.ByteOrder
import net.drthum.scalacraft.common.ByteIteratorExtensions._

case class AuthLogonProof(a: Array[Byte], clientHash: Array[Byte])

object AuthLogonProof {
  implicit val byteOrder = ByteOrder.nativeOrder

  def apply(bs: ByteString): Option[AuthLogonProof] = {
    val iter = bs.iterator
    if (iter.len >= 1 + 32 +20) {
      iter.drop(1) // cmd
      val a = iter.getBytes(32)
      val m1 = iter.getBytes(20)

      Some(AuthLogonProof(a, m1))
    } else
      None
  }
}
