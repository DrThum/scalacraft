package net.drthum.scalacraft.network

import akka.util.{ByteIterator, ByteStringBuilder, ByteString}
import java.nio.ByteOrder
import net.drthum.scalacraft.common.Opcodes._

object ClientPacket {
  implicit val byteOrder = ByteOrder.nativeOrder

  def parse(raw: ByteString) = new PacketParser(raw.iterator)
}

class PacketParser(iter: ByteIterator) {
  implicit val byteOrder = ByteOrder.nativeOrder

  val opcode = iter.getInt
  val size = iter.getShort

  def checkSize(size: Int) = iter.len // TODO: or length?

  def skip(n: Int) = iter.drop(n)
  def getByte = iter.getByte
  def getShort = iter.getShort
  def getInt = iter.getInt
  def getLong = iter.getLong
}

object ServerPacket {
  implicit val byteOrder = ByteOrder.nativeOrder

  def apply() = new PacketBuilder
  def apply(opcode: Short): PacketBuilder = {
    val builder = new PacketBuilder
    builder.setOpcode(opcode)
    builder
  }
}

class PacketBuilder {
  implicit val byteOrder = ByteOrder.nativeOrder

  private var opcode: Short = MSG_NULL_ACTION.toShort // TODO: Import implicit conversion from Int to Short
  private val body: ByteStringBuilder = new ByteStringBuilder

  def setOpcode(opcode: Short) = this.opcode = opcode
  // Size does not contain header size (4 for ServerPacket)
  def result: ByteString = ByteString(opcode) ++ ByteString(body.length.toShort) ++ body.result

  def <<(b: Byte): PacketBuilder = {
    body.putByte(b)
    this
  }

  def <<(s: Short) = {
    body.putShort(s)
    this
  }

  def <<(i: Int) = {
    body.putInt(i)
    this
  }

  def <<(l: Long) = {
    body.putLong(l)
    this
  }

  def <<(f: Float) = {
    body.putFloat(f)
    this
  }

  def <<(str: String) = {
    body.putBytes(str.getBytes)
    this
  }
}