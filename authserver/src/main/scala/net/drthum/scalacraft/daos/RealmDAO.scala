package net.drthum.scalacraft.daos

import services.DBService
import slick.driver.PostgresDriver.api._

import net.drthum.scalacraft.entities.Realms

object RealmDAO {
  val db = DBService.authDB
  val realms = TableQuery[Realms]

  def all = db.run(realms.result)
}
