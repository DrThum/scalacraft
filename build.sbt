name := "ScalaCraft"

version := "0.0.1"

scalaVersion := "2.10.3"

resolvers in ThisBuild += "Typesafe Releases Repository" at "http://repo.typesafe.com/typesafe/releases/"

resolvers in ThisBuild += "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

ideaExcludeFolders += ".idea"

ideaExcludeFolders += ".idea_modules"

scalacOptions in ThisBuild ++= Seq("-feature")

libraryDependencies in ThisBuild ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.3.2",
  "org.slf4j"           %  "slf4j-simple"      % "1.7.+",
  "com.typesafe.slick" %% "slick" % "3.0.0",
  "postgresql" % "postgresql" % "9.1-901.jdbc4",
  "com.zaxxer" % "HikariCP-java6" % "2.3.7"
)

fork in ThisBuild := true

lazy val common = project in file("common")

lazy val authserver = project in file("authserver") dependsOn(common)

lazy val proxy = project in file("proxyserver") dependsOn(common)

lazy val root = project in file(".") aggregate(authserver, proxy)

